let type = "WebGL"
if(!PIXI.utils.isWebGLSupported()){
    type = "canvas"
}
PIXI.utils.sayHello(type)

// Colors 
let BottomBarColor = 0x45ab79, 
    BottomBarLineColor = 0x90e9a5, 
    ButtonHoverColor = 0xdcdcba, 
    ButtonSpinColor = 0xa4deff, 
    ButtonNoBalanceColor = 0xc53c39, 
    ButtonBGColor = 0x6bbdab;

// Font 
let textStyle = new PIXI.TextStyle({
    fontFamily: "Ebrima",
    fontSize: 60,
    fontWeight: "bolder",
    letterSpacing: 2, 
    fill: "#f8f8c3"
  });

// Aliases 
let Application = PIXI.Application,
    Loader = PIXI.Loader.shared,
    Container = PIXI.Container, 
    Sprite = PIXI.Sprite, 
    Graphics = PIXI.Graphics, 
    Text = PIXI.Text;

let app = new Application({ 
    width: 2560, 
    height: 1440,                       
    antialias: true, 
    transparent: false, 
    resolution: 1
    }
);

// Logic fields
let balance = 1000;
let spinCost = 10;
let tileVariationsCount = 10;
let isSpinning = false;

// Visual fields
let spinButton;
let balanceText;
let tileHolder;
let tileColumns;
let tileMasker;
let tileSpinSpeed = 100;
let columnTileMultiplier = 7;
let tileWidth = 400;
let tileHeight = 336;
let isHoveringButton = false;

document.body.appendChild(app.view);

Loader
    .add("background", "images/game-background.jpg")
    .add("logo", "images/game-logo.png")
    .add("autoplay", "images/autoplay.png")
    .add("bet", "images/bet.png")
    .add("menu", "images/menu.png")
    .add("spin", "images/spin.png");

for(let i = 1; i <= tileVariationsCount; i++) {
    Loader.add("images/symbol" + i + ".png");
}

Loader.load(start);

function start() {
    drawBackground();
    drawBottomBar();
    createBoard();
    drawSpinButton();
    app.ticker.add(tileAnimator);
    app.ticker.add(buttonAnimator);
}

function drawBackground() {
    let bgHolder = new Container();
    bgHolder.position.set(app.screen.width * 0.5, app.screen.height * 0.5);
    app.stage.addChild(bgHolder);

    let bg = new Sprite(Loader.resources.background.texture);
    bg.anchor.set(0.5, 0.5);
    bgHolder.addChild(bg);

    let logo = new Sprite(Loader.resources.logo.texture);
    logo.anchor.set(0.5, 0.5);
    logo.position.y = app.screen.height * -0.43;
    bgHolder.addChild(logo);
}

function createBoard() {
    let scaleRatio = 1;
    let centerX = app.screen.width * 0.5;
    let centerY = app.screen.height * 0.5 - 30;
    let width = tileWidth * 5 * scaleRatio;
    let height = tileHeight * 3 * scaleRatio;

    tileMasker = new Graphics();
    tileMasker.beginFill(0xffffff, 1);
    tileMasker.drawRect(centerX - width * 0.5, centerY - height * 0.5, width, height);
    tileMasker.endFill();
    app.stage.addChild(tileMasker);

    tileColumns = new Container();
    tileColumns.mask = tileMasker;
    tileColumns.position.set(centerX - width * 0.5, centerY - height * 0.5);
    app.stage.addChild(tileColumns);
    for (let i = 0; i < 5; i++) {
        let column = new Container();
        tileColumns.addChild(column);
        column.position.set(i * tileWidth, tileHeight*2);
        column.vy = 0;
        addToColumn(column, 3);
    }
    tileColumns.scale.set(scaleRatio, scaleRatio);
}

function drawBottomBar() {
    let barThickness = app.screen.height * 0.07;
    let bottomBarHolder = new Container();
    bottomBarHolder.position.set(0, app.screen.height - barThickness * 0.5);
    app.stage.addChild(bottomBarHolder);

    let bottom = new Graphics();
    bottom.beginFill(BottomBarColor);
    bottom.lineStyle(5, BottomBarLineColor);
    bottom.drawRect(
        0, 
        barThickness * -0.5, 
        app.screen.width, 
        barThickness);
    bottom.endFill();
    bottom.alpha = 0.7;
    bottomBarHolder.addChild(bottom);

    balanceText = new Text("-", textStyle);
    balanceText.anchor.set(0, 0.5);
    balanceText.position.set(app.screen.width * 0.02, 0);
    bottomBarHolder.addChild(balanceText);

    let winText = new Text("-", textStyle);
    winText.anchor.set(0, 0.5);
    winText.position.set(app.screen.width * 0.6, 0);
    bottomBarHolder.addChild(winText);

    let betText = new Text("-", textStyle);
    betText.anchor.set(0, 0.5);
    betText.position.set(app.screen.width * 0.8, 0);
    bottomBarHolder.addChild(betText);

    // Hack solution so the correct font is loaded. 
    // Should be replaced by a proper Loader for PIXI.js. 
    setTimeout(function() {
        updateBalance();
        winText.text = "WIN  € 600.00";
        betText.text = "BET  € 400.00";
    }, 500);
}

function drawSpinButton() {
    let alpha = 0.9;
    spinButton = new Sprite(Loader.resources.spin.texture);
    spinButton.position.set(app.screen.width * 0.92, app.screen.height * 0.75);
    spinButton.anchor.set(0.5, 0.5);
    spinButton.alpha = alpha;
    spinButton.interactive = true;
    spinButton.buttonMode = true;
    spinButton
        .on("click", spin)
        .on("pointerover", () =>{isHoveringButton = true;})
        .on("pointerout", () =>{isHoveringButton = false;});
    app.stage.addChild(spinButton);

    let autoplay = new Sprite(Loader.resources.autoplay.texture);
    autoplay.position.set(app.screen.width * 0.92 - 140, app.screen.height * 0.75 + 140);
    autoplay.anchor.set(0.5, 0.5);
    autoplay.alpha = alpha;
    app.stage.addChild(autoplay);

    let bet = new Sprite(Loader.resources.bet.texture);
    bet.position.set(app.screen.width * 0.95, app.screen.height * 0.53);
    bet.anchor.set(0.5, 0.5);
    bet.alpha = alpha;
    app.stage.addChild(bet);

    let menu = new Sprite(Loader.resources.menu.texture);
    menu.position.set(app.screen.width * 0.06, app.screen.height * 0.8);
    menu.anchor.set(0.5, 0.5);
    menu.alpha = alpha;
    app.stage.addChild(menu);
}

function spin() {
    if (isSpinning) {
        console.log("Already spinning");
        return;
    }
    if (balance - spinCost < 0) console.error("Should not be able to trigger: spin()");
    balance -= spinCost;
    updateBalance();
    for (let i = 0; i < 5; i++) {
        let column = tileColumns.children[i];
        column.vy = tileSpinSpeed;
        addToColumn(column, 6 + columnTileMultiplier * i);
    }
    isSpinning = true;
}

function updateBalance() {
    balanceText.text = "BALANCE  € " + balance.toFixed(2);
}

function addToColumn(column, amount) {
    for (let i = 0; i < amount; i++) {
        let tile = createTile(Math.ceil(Math.random() * tileVariationsCount));
        if (column.children.length > 0) {
            tile.position.set(0, column.children[column.children.length - 1].position.y - tile.height);
        }
        column.addChild(tile);
    }
}

function resetColumn(column) {
    let amountToRemove = column.children.length - 3;
    for (let i = 0; i < amountToRemove; i++) {
        column.removeChildAt(0);
    }
    let previousTile = column.children[0];
    previousTile.position.set(0, -previousTile.height);
    for (let i = 1; i < column.children.length; i++) {
        let currentTile = column.children[i];
        currentTile.position.set(0, previousTile.position.y - currentTile.height);
        previousTile = currentTile;
    }
    column.vy = 0;
    column.position.y = -column.children[column.children.length - 1].position.y;
}

function createTile(index) {
    let tile = new Sprite(Loader.resources["images/symbol" + index + ".png"].texture);
    return tile;
}

function tileAnimator(delta) {
    if (!isSpinning) return;
    let stoppedCounter = 0;
    for (let i = 0; i < 5; i++) {
        let column = tileColumns.children[i];
        if (column.vy == 0) {
            stoppedCounter++
            continue;
        }
        column.position.y += column.vy * delta;
        if (column.position.y > -(column.children[column.children.length - 1].position.y)) {
            resetColumn(column);
        }
    }
    if (stoppedCounter == 5) isSpinning = false;
}

function buttonAnimator() {
    if (isSpinning) {
        spinButton.tint = ButtonSpinColor;
        spinButton.interactive = false;
    }
    else if (balance - spinCost < 0) {
        spinButton.tint = ButtonNoBalanceColor;
        spinButton.interactive = false;
    }
    else if (isHoveringButton) {
        spinButton.tint = ButtonHoverColor;
        spinButton.interactive = true;
    }
    else {
        spinButton.tint = 0xffffff;
        spinButton.interactive = true;
    }
}
